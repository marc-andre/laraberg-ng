<?php
declare(strict_types=1);

return [
    'use_package_routes' => true,
    'middlewares' => ['web', 'auth'],
    'prefix' => 'laraberg',
    "models" => [
        "block"   => MarcAndreAppel\LarabergNG\Models\Block::class,
        "content" => MarcAndreAppel\LarabergNG\Models\Content::class,
    ],
];
